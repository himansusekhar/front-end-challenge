import React, { Component } from 'react';
import './header.css'
import Img from 'react-image'
import logo from  '../../assets/logo.png'
import SearchAppBar from '../Hooks/SearchJobBar'
import SelectJobsByExperience from '../Hooks/SelectJobsByExperience'
import SelectJobsByLocation from '../Hooks/SelectJobsByLocation'
import SelectJobsBySkills from '../Hooks/SelectJobsBySkills'

export default class Header extends Component {

	constructor(props) {
		super(props);
		this.state ={
			toggle: false
		}
	}

	getSelectedSkills = (filterKey, searchCriteria) => {
		this.props.expiredFilter(filterKey, searchCriteria)
	}

	handleOnlickFilter = () => {
		this.setState({toggle: !this.state.toggle }, 
			() => this.props.expiredFilter(this.state.toggle && 'EXPIRED'))
	}

	render(){
		return(
		<div className="header-theme">
			<div className='logo-style' onClick={ () => { this.setState({toggle: false}); this.props.expiredFilter() }}><Img src={logo} height = '90px' /></div>
			<div className="menu-style">
				<div style={{color: this.state.toggle ? '#ef1c74' : '#ffffff' }} className='expiring-jobs' onClick={ () => this.handleOnlickFilter() }>Expired Jobs</div>
				<SelectJobsByExperience 
					experience={this.props.experience} 
					getSelectedExperienceCallback = { 
						(experience) => this.getSelectedSkills('EXPERIENCE', experience) 
					} 
				/>

				<SelectJobsByLocation 
					cities={this.props.cities} 
					getSelectedLocationCallback = { 
						(location) => this.getSelectedSkills('LOCATION', location) 
					}
				/>
				<SelectJobsBySkills  
					skills={this.props.skills}  
					getSelectedSkillsCallback = { 
						(skills) => this.getSelectedSkills('SKILLS', skills) 
					}
				/>
				
				<SearchAppBar/>
			
			</div>
		 </div>
		)
	}
}


