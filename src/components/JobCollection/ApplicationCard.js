import React, { Component } from 'react';
import './application-card.css'
export default class ApplicationCard extends Component {

	render(){
		const notMentioned = 'Not mentioned'
		const {title, experience, location, companyname, applylink, salary, skills, source, jd, type, enddate, created}  = this.props.listData
		return(
			<div key={this.props.index} className='card-top-view'>
				<div><span className='card-title'>Title: </span> <span className='card-title-values'>{title || notMentioned}</span></div>
				<div><span className='card-title'>Company Name: </span> <span className='card-title-values'>{companyname || notMentioned}</span></div>
				<div><span className='card-title'>Experience: </span> <span className='card-title-values'>{experience || notMentioned }</span></div>
				<div><span className='card-title'>Location: </span> <span className='card-title-values'>{location || notMentioned}</span></div>
				<div><span className='card-title'>Skills: </span> <span className='card-title-values'>{ skills || notMentioned }</span></div>
				<div><span className='card-title'>salary: </span> <span className='card-title-values'>{salary || notMentioned}</span></div>
				<div><span className='card-title'>Type: </span> <span className='card-title-values'>{type || notMentioned }</span></div>
				<div><span className='card-title'>Source: </span> <span className='card-title-values'>{source || notMentioned}</span></div>
				<div><span className='card-title'>JD: </span> <span className='card-title-values'>{jd || notMentioned}</span></div>
				<div><span className='card-title'>Created: </span> <span className='card-title-values'>{created || notMentioned }</span></div>
				<div><span className='card-title'>End date: </span> <span className='card-title-values'>{enddate || notMentioned}</span></div>
				<div><a target="_blank" rel="apply" className='apply-button-div' href={applylink}> <span style={{ margin: 'auto' }}>APPLY</span></a></div>
			</div>
		)
	}
}