import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import {useState} from '../../helper/helper';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	formControl: {
		minWidth: 140,
		maxWidth: 140,
	},
	chips: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	chip: {
		margin: 2,
	},
	noLabel: {
		marginTop: theme.spacing(3),
	},
}));

export default function SelectJobsBySkills({skills, getSelectedSkillsCallback}) {
	const classes = useStyles();
	const [skillsName, setskillsName] = useState([], skillsName => getSelectedSkillsCallback(skillsName))
	
	return (
		<div className={classes.root}>
			<FormControl className={classes.formControl}>
				<InputLabel htmlFor="select-multiple-checkbox">Select Skills</InputLabel>
				<Select
					multiple
					value={skillsName}
					onChange={(event) => setskillsName(event.target.value)}
					input={<Input id="select-multiple-checkbox" />}
					renderValue={selected => selected.join(', ')}
					>
					{skills && skills.map(name => (
						<MenuItem key={name} value={name}>
							<Checkbox checked={skillsName.indexOf(name) > -1} />
							<ListItemText primary={name} />
						</MenuItem>
					))}
				</Select>
			</FormControl>
		</div>
	);
}