import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import {useState} from '../../helper/helper'
const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	formControl: {
		minWidth: 140,
		maxWidth: 140,
	},
	chips: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	chip: {
		margin: 2,
	},
	noLabel: {
		marginTop: theme.spacing(3),
	},
}));

export default function SelectJobsByExperience({experience, getSelectedExperienceCallback}) {

	const classes = useStyles();
	const [yearsOfExperience, setYearsOfExperience] = useState([], yearsOfExperience => getSelectedExperienceCallback(yearsOfExperience));
	
	return (
	<div className={classes.root}>
		<FormControl className={classes.formControl}>
			<InputLabel htmlFor="select-multiple-checkbox">Select Experience</InputLabel>
			<Select
				multiple
				value={yearsOfExperience}
				onChange={(event) => setYearsOfExperience(event.target.value)}
				input={<Input id="select-multiple-checkbox" />}
				renderValue={selected => selected.join(', ')}
				>
				{experience && experience.map(name => (
					<MenuItem key={name} value={name}>
						<Checkbox checked={yearsOfExperience.indexOf(name) > -1} />
						<ListItemText primary={name} />
					</MenuItem>
				))}
			</Select>
		</FormControl>
	</div>
);
}