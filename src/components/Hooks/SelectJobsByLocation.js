import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import {useState} from '../../helper/helper';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	formControl: {
		minWidth: 140,
		maxWidth: 140,
	}
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
			width: 250,
		},
	},
};

export default function SelectJobsByLocation({cities, getSelectedLocationCallback}) {

	const classes = useStyles();
	const [location, setLocation] = useState([], location => getSelectedLocationCallback(location))
	
	return (
		<div className={classes.root}>
			<FormControl className={classes.formControl}>
				<InputLabel htmlFor="select-multiple-checkbox">Select Location</InputLabel>
				<Select
					multiple
					value={location}
					onChange={(event) => setLocation(event.target.value) }
					input={<Input id="select-multiple-checkbox" />}
					renderValue={selected => selected.join(', ')}
					MenuProps={MenuProps}
					>
					{cities && cities.map(name => (
						<MenuItem key={name} value={name}>
							<Checkbox checked={location.indexOf(name) > -1} />
							<ListItemText primary={name} />
						</MenuItem>
					))}
				</Select>
			</FormControl>
		</div>
	);
}