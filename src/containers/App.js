import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getJobsData } from '../actions';
import Loader from '../components/static_js/Loader'
import Header from '../components/Header/Header'
import ApplicationCard from '../components/JobCollection/ApplicationCard'
import { bindActionCreators } from 'redux';

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			jobsCollection: []
		}
	}

	componentDidMount(){
		const { getJobsData } = this.props;
		getJobsData();
	}

	handleFilter = (status, criteria) => {
		const { oppertunities, expiredJobList, experienceObjList, citiesObjList, skillsObjList } = this.props.jobReducer;

		let filterExperience = []
		if(status === 'EXPIRED'){
			filterExperience = expiredJobList;
		}
		else if(status === 'EXPERIENCE' && Array.isArray(experienceObjList) && Array.isArray(criteria)){
			filterExperience = experienceObjList.filter((item) =>  criteria.includes(item.experience))
		}
		else if(status === 'SKILLS' && Array.isArray(skillsObjList) && Array.isArray(criteria)){
			filterExperience = skillsObjList.filter((item) =>  criteria.includes(item.skills))
		}
		else if(status === 'LOCATION' && Array.isArray(citiesObjList) && Array.isArray(criteria)){
			filterExperience = citiesObjList.filter((item) =>  criteria.includes(item.location))
		}else{
			filterExperience = oppertunities
		}

		this.setState({jobsCollection: filterExperience })

	}

	componentDidUpdate(prevProps){

		const { loadingList, oppertunities } = this.props.jobReducer;

		if(prevProps.jobReducer && prevProps.jobReducer.loadingList && !loadingList){
			this.setState({jobsCollection: oppertunities})
		}

	}

	render() {
		const { loadingList, cities, skills, experience } = this.props.jobReducer;

		if(loadingList) return <Loader/>

		return (
			<div>
				<Header
					cities={cities}
					skills={skills}
					experience={experience}
					expiredFilter = { (status, criteria) => this.handleFilter(status, criteria) }
					/>
				<div style={{ padding: '90px 0' }}>
					{
						this.state.jobsCollection.map((data, index) => <ApplicationCard key={index} listData={data}/>)
					}
				</div>
			</div>
		);
	}
}

const  mapStateToProps = (state) => {
	const { jobReducer } = state;
	return {
		jobReducer
	};
}

const mapDispatchToProps = dispatch => bindActionCreators({
	  getJobsData,
	}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App);
