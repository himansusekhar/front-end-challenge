import React from 'react';

export const useState = (initialState, callback = () => { } ) => {
    const [ state, setState ] = React.useState(initialState)
    const totalCalls = React.useRef(0)
    React.useEffect(() => {
      if (totalCalls.current < 1) {
        totalCalls.current += 1
        return
      }
      callback(state)
    }, [state])

    return [ state, setState ]
  }