import { combineReducers } from 'redux';
import {
		GET_JOB_LIST,
		GET_JOB_LIST_SUCCESS,
		GET_JOB_LIST_ERROR
} from './constants/ActionTypes';

const initialState = {
	loadingList: false,
	result: {}
}

const getJobList = (state) => {
	return {
		...state,
		loadingList: true,
	}
}

const setJobListData = (state, action) => ({
	...state,
	loadingList: false,
	oppertunities: Array.isArray(action.result.data) && action.result.data.length ? action.result.data : [],
	totalItemCount: action.result.len ? action.result.len : 0,
	...action
})

const setErrorMessage = (state, action) => ({
	 ...state,
		loadingList: false,
		error: action.error,
})


export function jobReducer( state = initialState, action ) {

	switch (action.type) {
		case GET_JOB_LIST:
				return getJobList(state);
		case GET_JOB_LIST_SUCCESS:
				return setJobListData(state, action);
		case GET_JOB_LIST_ERROR:
				return setErrorMessage(state, action);
		default:
				return state;
	}

}

const rootReducer = combineReducers({
	jobReducer,
});

export default rootReducer;
