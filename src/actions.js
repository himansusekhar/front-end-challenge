import {
	GET_JOB_LIST,
	GET_JOB_LIST_SUCCESS,
	GET_JOB_LIST_ERROR,
} from './constants/ActionTypes';

const requests = require('superagent');
const moment = require('moment')

const requestJobsList = () => ({
		type: GET_JOB_LIST,
	})

const  getCitiesSkillsAndExperinceList = (responseItem) => {

	let cities = [];
	let skills = [];
	let experience = []
	let expiredJobList = []
	let experienceObjList = []
	let citiesObjList = [];
	let skillsObjList = [];

	if(responseItem && Array.isArray(responseItem.data) && responseItem.data.length){
		responseItem.data.map(item => {

			if(item.location){
				!cities.includes(item.location) && cities.push(item.location)
				citiesObjList.push(item)
			}

			if(item.skills){
				!skills.includes(item.skills) && skills.push(item.skills);
				skillsObjList.push(item)
			}

			if(item.experience){ 
				!experience.includes(item.experience) && experience.push(item.experience)
				experienceObjList.push(item);
			}

			if(item.enddate && moment().isAfter(item.enddate)) expiredJobList.push(item)
		})
	}

	return {cities, skills, experience, expiredJobList, experienceObjList, citiesObjList, skillsObjList}
}


const responseJobsList = (responseItem) => ({
	type: GET_JOB_LIST_SUCCESS,
	result: responseItem,
	...getCitiesSkillsAndExperinceList(responseItem)
	})

const responseJobListError = (error) => ({
	type: GET_JOB_LIST_ERROR,
	error,
})

export const getJobsData = () => (dispatch, getState) => {
  dispatch(requestJobsList());
  requests.get(`https://nut-case.s3.amazonaws.com/jobs.json`)
  .set('Accept', 'application/json')
  .end((err, res) => {
	if(err) return dispatch(responseJobListError(err))
	dispatch(responseJobsList(res.body))
  })
}
